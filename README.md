# SonarQube

В данном репозитории находится информация про то, как установить, настроить SonarQube и как интегрировать его с GitLab

## Установка

[Документация SonarQube](https://docs.sonarqube.org/latest/setup-and-upgrade/install-the-server/)

[Пример docker-compose](docker-compose.yml)

## Настройка

[Интеграция с GitLab](https://docs.sonarqube.org/latest/devops-platform-integration/gitlab-integration/)

Краткая инструкция:

1. По первой ссылке в разделе установки установить где-то на сервере SonarQube, например, через docker compose
2. Зайти через веб-интерфейс по порту, указанному в docker-compose.yml
3. Далее перейти по пути Administration -> ALM Integrations -> GitLab, нажать кнопку Create configuration. ![скрин](.doc/img.png)
4. Вводим любое название, путь до API гитлаба (https://gitlab-example.com/api/v4), вводим токен пользователя (лучше системного), который имеет доступ к нужным репозиториям. Нужны права api, read_api и read_repository
5. Далее на главной странице нажимаем на Add project, выбираем гитлаб
   ![скрин](.doc/img2.png)
6. Выбираем проект и действуем по инструкции (добавляем ключи в переменные окружения (SONAR_HOST_URL и SONAR_TOKEN), добавляем задачу в CI/CD). Пример конфига ci/cd приведён в [.gitlab-ci.yml](.gitlab-ci.yml)
